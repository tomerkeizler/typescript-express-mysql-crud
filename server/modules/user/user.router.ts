/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper";
import {connection as db} from "../../db/mysql.connection";
import express, { Request,Response } from "express";
import { optional_user_schema, required_user_schema } from "./user.schema";
import validation_handler from "../../middleware/validation.handler";
import {OkPacket,RowDataPacket} from "mysql2/promise";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW USER
router.post(
  "/",
  validation_handler(required_user_schema),
  raw(async (req: Request, res: Response) => {
    console.log(req.body, "create a user, req.body:");

    const sql = `INSERT INTO users SET ?`;
    const results = await db.query(sql, req.body);
    const result:OkPacket = results[0] as OkPacket;
    
    if (result.affectedRows) {
      res.status(200).json(`User added successfully`);
    } else {
      res.status(404).json({ status: `Error while adding user` });
    }
  })
);

// GETS ALL USERS
router.get(
  "/",
  raw(async (req: Request, res: Response) => {
    const sql = `SELECT * FROM users`;
    const [rows] = await db.query(sql);
    res.status(200).json(rows);
  })
);

// GETS ALL USERS - WITH PAGINATION
router.get(
  "/paginate/:page?/:items?",
  raw(async (req: Request, res: Response) => {
    let { page = '0', items = '10' } = req.params;
    const sql = `
    SELECT * FROM users
    LIMIT ${parseInt(items)} 
    OFFSET ${parseInt(page) * parseInt(items)}`;
    const [rows] = await db.query(sql);
    res.status(200).json(rows);
  })
);

// GETS A SINGLE USER
router.get(
  "/:id",
  raw(async (req: Request, res: Response) => {
    const sql = `SELECT * FROM users WHERE id = ${req.params.id}`;
    const results:RowDataPacket = await db.query(sql) as RowDataPacket;
    const rows = results[0][0][0] = await db.query(sql);

    if (rows.length > 0) {
      res.status(200).json(rows[0]);
    } else {
      res.status(404).json({ status: `User ${req.params.id} was not found` });
    }
  })
);

// UPDATES A FULL SINGLE USER
router.put(
  "/:id",
  validation_handler(required_user_schema),
  raw(async (req: Request, res: Response) => {
    const { first_name, last_name, email, phone } = req.body;
    const sql = `UPDATE users SET first_name='${first_name}', last_name='${last_name}', email='${email}', phone='${phone}' WHERE id = ${req.params.id}`;
    const results = await db.query(sql);
    const result:OkPacket = results[0] as OkPacket;

    if (result.affectedRows) {
      res.status(200).json("User was updated successfully");
    } else {
      res.status(404).json({ status: `User ${req.params.id} was not found` });
    }
  })
);

// DELETES A USER
router.delete(
  "/:id",
  raw(async (req: Request, res: Response) => {
    const sql = `DELETE FROM users WHERE id = ${req.params.id}`;
    const results = await db.query(sql);
    const result:OkPacket = results[0] as OkPacket;

    if (result.affectedRows) {
      res.status(200).json("User was deleted successfully");
    } else {
      res.status(404).json({ status: `User ${req.params.id} was not found` });
    }
  })
);

export default router;
