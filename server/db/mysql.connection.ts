import mysql from "mysql2/promise";
import env from "../utils/util.env";

export let connection: mysql.Connection;

export const connect = async () => {
  if (connection) return connection;
  connection = await mysql.createConnection({
    host: env("DB_HOST"),
    port: Number(env("DB_PORT")),
    database: env("DB_NAME"),
    user: env("DB_USER_NAME"),
    password: env("DB_USER_PASSWORD"),
  });
  await connection.connect();
  console.log(" ✨  Connected to MySQL ✨ ");
};
