import { Request, Response, NextFunction } from 'express'
import {RequestHandler,ErrorRequestHandler} from 'express';
import env from "../utils/util.env"

// export const error_handler =  (err: any, req: Request, res: Response, next: NextFunction) => {
export const error_handler: ErrorRequestHandler = (err, req, res, next) => {
    console.log(err);
    next(err)
}

// export const error_handler2 =  (err: any, req: Request, res: Response, next: NextFunction) => {
export const error_handler2: ErrorRequestHandler = (err, req, res, next) => {
    if(env('NODE_ENV') !== 'production')res.status(500).json({status:err.message,stack:err.stack});    
    else res.status(500).json({status:'internal server error...'});
}

// export const not_found =  (req: Request, res: Response) => {
export const not_found: RequestHandler =  (req, res) => {
    console.log(`url: ${req.url} not found...`);
    res.status(404).json({status:`url: ${req.url} not found...`});
}


