import { Request, Response, NextFunction } from 'express'

const validation_handler = (schema: any) => {
  return (req: Request, res: Response, next: NextFunction) => {
    const { error } = schema.validate(req.body);

    if (error == null) next();
    else {
      const message = error.details.map((err: any) => err.message).join(",");
      res.status(422).json({ error: message });
    }
  };
};

export default validation_handler;
