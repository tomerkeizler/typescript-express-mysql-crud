const env = (key: string): string => {
    const value = process.env[key]
    if(!value) throw new Error(`missing process.env[${key}]`)
    return value;
}

export default env
