import express from 'express'
import morgan from 'morgan'
import cors from 'cors'

import user_router from './modules/user/user.router';
import {error_handler,error_handler2,not_found} from './middleware/errors.handler';   
import env from "./utils/util.env"
import { connect } from "./db/mysql.connection"

const PORT = Number(env('PORT'))
const HOST = env('HOST')

const app = express();

// middleware
app.use(cors());
app.use(morgan('dev'))

// routing
// app.use('/api/stories', story_router);
app.use('/api/users', user_router);

// central error handling
app.use(error_handler);
app.use(error_handler2);

//when no routes were matched...
app.use('*', not_found)

//start the express api server
;(async ()=> {
  await connect() //connect to mySQL
  await app.listen(PORT,HOST);
  console.log(`api is live on`,` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
})().catch(console.log)